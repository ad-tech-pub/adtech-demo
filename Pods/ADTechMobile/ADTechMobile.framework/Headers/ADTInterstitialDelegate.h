//
//  ADTInterstitialDelegate.h
//  ADTechMobile
//
//  Created by Kyle M on 12/18/19.
//  Copyright © 2019 ADTech. All rights reserved.
//

#ifndef ADTInterstitialDelegate_h
#define ADTInterstitialDelegate_h

@class ADTInterstitial;

@protocol ADTInterstitialDelegate <NSObject>
@optional

/// Tell delegate the interstitial ad did receive ads
- (void)adtInterstitialDidReceiveAd:(ADTInterstitial *)ad;

/// Tell delegate the ad request has failed. The error object will tell the reason why
- (void)adtInterstitial:(ADTInterstitial *)ad didFailToReceiveAdWithError:(NSError *)error;

/// Tell delegate a new ad screen will present
- (void)adtInterstitialWillPresentScreen:(ADTInterstitial *)ad;

/// Tell delegate the new ad screen failed to present
- (void)adtInterstitialDidFailToPresentScreen:(ADTInterstitial *)ad;

/// Tell delegate the ad screen will dismiss
- (void)adtInterstitialWillDismissScreen:(ADTInterstitial *)ad;

/// Tell delegate the ad screen did dismiss
- (void)adtInterstitialDidDismissScreen:(ADTInterstitial *)ad;

/// Tell the delegate that the app will go to background
- (void)adtInterstitialWillLeaveApplication:(ADTInterstitial *)ad;

@end

#endif /* ADTInterstitialDelegate_h */
