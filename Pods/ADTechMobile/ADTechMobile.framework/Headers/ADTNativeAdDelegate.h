//
//  ADTNativeAdDelegate.h
//  ADTechMobile
//
//  Created by Kyle M on 12/12/19.
//  Copyright © 2019 ADTech. All rights reserved.
//

#ifndef ADTNativeAdDelegate_h
#define ADTNativeAdDelegate_h

@class ADTNativeAd;

@protocol ADTNativeAdDelegate <NSObject>
@optional

/// Tell delegate native ad did receive ad
- (void)adtNativeAdDidReceiveAd:(ADTNativeAd *)ad;

/// Tell delegate native ad has failed
- (void)adtNativeAd:(ADTNativeAd *)ad didFailToReceiveAdWithError:(NSError *)error;

/// Tell delegate that native ad did record impression from showing the ad
- (void)adtNativeAdDidRecordImpression:(ADTNativeAd *)ad;

/// Tell delegate native ad did finish loading all assets
- (void)adtNativeAdDidFinishLoading:(ADTNativeAd *)ad;

/// Tell delegate that native ad did record user tap
- (void)adtNativeAdDidRecordClick:(ADTNativeAd *)ad;

/// Tell delegate that native ad will present a new ad screen
- (void)adtNativeAdWillPresentScreen:(ADTNativeAd *)ad;

/// Tell delegate that native ad did successfully present new ad screen
- (void)adtNativeAdDidPresentScreen:(ADTNativeAd *)ad;

/// Tell delegate that native ad will dismiss the ad screen being displayed
- (void)adtNativeAdWillDismissScreen:(ADTNativeAd *)ad;

/// Tell delegate that native ad did dismiss the ad screen
- (void)adtNativeAdDidDismissScreen:(ADTNativeAd *)ad;

/// Tell delegate that application will go to background, and transition to a new app
- (void)adtNativeAdWillLeaveApplication:(ADTNativeAd *)ad;

@end

#endif /* ADTNativeAdDelegate_h */
