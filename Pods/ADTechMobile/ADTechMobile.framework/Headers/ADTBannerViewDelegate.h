//
//  ADTBannerViewDelegate.h
//  ADTechMobile
//
//  Created by Kyle M on 12/18/19.
//  Copyright © 2019 ADTech. All rights reserved.
//

#ifndef ADTBannerViewDelegate_h
#define ADTBannerViewDelegate_h

@class ADTBannerView;

@protocol ADTBannerViewDelegate <NSObject>
@optional

/// Tell delegate the banner view did receive ads
- (void)adtBannerViewDidReceiveAd:(ADTBannerView *)bannerView;

/// Tell delegate the ad request has failed. The error object will tell the reason why
- (void)adtBannerView:(ADTBannerView *)bannerView didFailToReceiveAdWithError:(NSError *)error;

/// Tell the delegate the ad screen that launched by the banner view will dismiss
- (void)adtBannerViewWillDismissScreen:(ADTBannerView *)bannerView;

/// Tell the delegate the ad screen that launched by the banner view did dismiss
- (void)adtBannerViewDidDismissScreen:(ADTBannerView *)bannerView;

/// Tell the delegate that the banner view will launch an ad full screen
- (void)adtBannerViewWillPresentScreen:(ADTBannerView *)bannerView;

/// Tell the delegate that the banner view did launch an ad full screen
- (void)adtBannerViewDidPresentScreen:(ADTBannerView *)bannerView;

/// Tell the delegate that the app will go to background
- (void)adtBannerViewWillLeaveApplication:(ADTBannerView *)bannerView;

@end

#endif /* ADTBannerViewDelegate_h */
