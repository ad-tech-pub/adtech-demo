//
//  ADTRewardedAd.h
//  ADTechMobile
//
//  Created by Kyle M on 7/24/20.
//  Copyright © 2020 ADTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ADTRewardedAdDelegate;

@interface ADTRewardedAd : NSObject

/// The delegate which will receive ad load events
@property (weak, nonatomic, nullable) id <ADTRewardedAdDelegate> delegate;

/// Returns TRUE if network has ad, and loaded. Otherwise returns FALSE.
@property (nonatomic, readonly) BOOL isReady;

/**
 Request the ad from network.
 */
- (void)request;

/**
 Present the ad using the root view controller. Before calling this method, check if the ad is ready with isReady.
 @param rootViewController The view controller that will present the interstitial ad.
 */
- (void)presentFromRootViewController:(nonnull UIViewController *)rootViewController;

@end
