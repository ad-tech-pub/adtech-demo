//
//  ADTInterstitialAd.h
//  ADTechMobile
//
//  Created by Kyle M on 11/7/19.
//  Copyright © 2019 ADTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ADTInterstitialDelegate;

@interface ADTInterstitial : NSObject

/// The delegate which will receive ad load events
@property (weak, nonatomic, nullable) id <ADTInterstitialDelegate> delegate;

/// Returns TRUE if network has ad, and loaded. Otherwise returns FALSE.
@property (nonatomic, readonly) BOOL isReady;

/**
 Request the ad from network.
 */
- (void)request;

/**
 Present the ad using the root view controller. Before calling this method, check if the ad is ready with isReady.
 @param rootViewController The view controller that will present the interstitial ad.
 */
- (void)presentFromRootViewController:(nonnull UIViewController *)rootViewController;


@end

