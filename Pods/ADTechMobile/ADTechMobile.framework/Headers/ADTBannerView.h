//
//  ADTBannerAdView.h
//  ADTechMobile
//
//  Created by Kyle M on 10/23/19.
//  Copyright © 2019 ADTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <ADTechMobile/ADTAdSize.h>

@protocol ADTBannerViewDelegate;

@interface ADTBannerView : UIView

/// Set rootViewController to the current view controller at the time this method is called.
@property (weak, nonatomic) UIViewController *rootViewController;

/// The delegate which will receive ad load events
@property (weak, nonatomic) id <ADTBannerViewDelegate> delegate;

/// Banner view size
@property (nonatomic) ADTAdSize adSize;

/**
 Request the ad from network.
 */
- (void)request;

@end

