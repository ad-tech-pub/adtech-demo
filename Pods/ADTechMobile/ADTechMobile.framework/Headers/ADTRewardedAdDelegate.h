//
//  ADTRewardedAdDelegate.h
//  ADTechMobile
//
//  Created by Kyle M on 7/24/20.
//  Copyright © 2020 ADTech. All rights reserved.
//

#ifndef ADTRewardedAdDelegate_h
#define ADTRewardedAdDelegate_h

@class ADTRewardedAd;

@protocol ADTRewardedAdDelegate <NSObject>
@optional

/// Tell delegate the interstitial ad did receive ads
- (void)adtRewardedAdDidReceiveAd:(ADTRewardedAd *)rewardedAd;

/// Tell delegate the ad request has failed. The error object will tell the reason why
- (void)adtRewardedAd:(ADTRewardedAd *)rewardedAd didFailToReceiveAdWithError:(NSError *)error;

/// Tells the delegate that the user earned a reward.
- (void)adtRewardedAdDidEarnReward:(ADTRewardedAd *)rewardedAd;

/// Tells the delegate that the rewarded ad was presented.
- (void)adtRewardedAdDidPresent:(ADTRewardedAd *)rewardedAd;

/// Tells the delegate that the rewarded ad failed to present.
- (void)adtRewardedAd:(ADTRewardedAd *)rewardedAd didFailToPresentWithError:(NSError *)error;

/// Tells the delegate that the rewarded ad was dismissed.
- (void)adtRewardedAdDidDismiss:(ADTRewardedAd *)rewardedAd;

@end

#endif /* ADTRewardedAdDelegate_h */
