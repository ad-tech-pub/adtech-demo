//
//  ADTNativeAssetIdentifier.h
//  ADTechMobile
//
//  Created by Kyle M on 12/14/19.
//  Copyright © 2019 ADTech. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NSString *ADTNativeAdAssetIdentifier NS_STRING_ENUM;

extern ADTNativeAdAssetIdentifier _Nonnull const ADTNativeAdHeadlineAsset;
extern ADTNativeAdAssetIdentifier _Nonnull const ADTNativeAdCallToActionAsset;
extern ADTNativeAdAssetIdentifier _Nonnull const ADTNativeAdIconAsset;
extern ADTNativeAdAssetIdentifier _Nonnull const ADTNativeAdBodyAsset;
extern ADTNativeAdAssetIdentifier _Nonnull const ADTNativeAdStoreAsset;
extern ADTNativeAdAssetIdentifier _Nonnull const ADTNativeAdPriceAsset;
extern ADTNativeAdAssetIdentifier _Nonnull const ADTNativeAdAdvertiserAsset;
extern ADTNativeAdAssetIdentifier _Nonnull const ADTNativeAdMediaViewAsset;
