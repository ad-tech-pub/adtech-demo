//
//  ADTAdSize.h
//  ADTechMobile
//
//  Created by Kyle M on 7/4/20.
//  Copyright © 2020 ADTech. All rights reserved.
//

typedef NS_ENUM(NSInteger, ADTAdSize) {
    /**
     Minimum width 320, height 50
     */
    height_50,
    
    /**
     Minimum width 728, height 90
     */
    height_90,
    
    /**
     Minimum width 300, height 250
     */
    height_250
};
