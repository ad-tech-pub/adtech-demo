//
//  ADTNativeAd.h
//  ADTechMobile
//
//  Created by Kyle M on 12/12/19.
//  Copyright © 2019 ADTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <ADTechMobile/ADTNativeAdAssetIdentifier.h>

@protocol ADTNativeAdDelegate;

@interface ADTNativeAd : NSObject

/// The delegate which will receive the native ad object or error
@property (weak, nonatomic, nullable) id <ADTNativeAdDelegate> delegate;

/// Call to action text. This usually is the text for action button.
@property (nonatomic, readonly, nullable) NSString *callToAction;

/// Headline text
@property (nonatomic, readonly, nullable) NSString *headline;

/// Body text. The ad main description
@property (nonatomic, readonly, nullable) NSString *body;

/// Store promotion text
@property (nonatomic, readonly, nullable) NSString *store;

/// Price info
@property (nonatomic, readonly, nullable) NSString *price;

/// Advertiser info
@property (nonatomic, readonly, nullable) NSString *advertiser;

/// Icon image for the ad
@property (nonatomic, readonly, nullable) UIImage *icon;

/// Icon image url. If image icon not available, there could be a url for the icon instead.
@property (nonatomic, readonly, nullable) NSURL *iconURL;

/**
 Request the ad from networks.
 */
- (void)request;

/**
 When the ad space is no longer needed, call this method to release.
 */
- (void)unregisterAd;

/**
 Register the native ad assets
 @param adView The native ad container view.
 @param mediaView The view that will display image or video for the ad.
 @param viewController That is used to present ads
 @param clickableAssets Tell native ad which view should be clickable
 */
- (void)registerAdView:(nonnull UIView *)adView
             mediaView:(nonnull UIView *)mediaView
        viewController:(nonnull UIViewController *)viewController
        clickableViews:(nonnull NSDictionary <ADTNativeAdAssetIdentifier, UIView *> *)clickableAssets;

/**
 Load an image from a url asynchronously
 @param imageUrl The url to load image from
 @param block The completion block to handle the image once it is downloaded.
 */
- (void)loadImageAsync:(nonnull NSURL *)imageUrl completion:(nullable void (^)(UIImage * __nullable image))block;

@end
