//
//  ADTechMobile.h
//  ADTechMobile
//
//  Copyright © 2019 ADTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <ADTechMobile/ADTInterstitial.h>
#import <ADTechMobile/ADTBannerView.h>
#import <ADTechMobile/ADTNativeAd.h>
#import <ADTechMobile/ADTNativeAdDelegate.h>
#import <ADTechMobile/ADTNativeAdAssetIdentifier.h>
#import <ADTechMobile/ADTBannerViewDelegate.h>
#import <ADTechMobile/ADTInterstitialDelegate.h>
#import <ADTechMobile/ADTRequestConfiguration.h>
#import <ADTechMobile/ADTAdSize.h>
#import <ADTechMobile/ADTRewardedAd.h>
#import <ADTechMobile/ADTRewardedAdDelegate.h>

//! Project version number for ADTechMobile.
FOUNDATION_EXPORT double ADTechMobileVersionNumber;

//! Project version string for ADTechMobile.
FOUNDATION_EXPORT const unsigned char ADTechMobileVersionString[];
