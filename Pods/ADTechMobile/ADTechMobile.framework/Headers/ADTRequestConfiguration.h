//
//  ADTRequestConfiguration.h
//  ADTechMobile
//
//  Created by Kyle M on 1/10/20.
//  Copyright © 2020 ADTech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADTRequestConfiguration : NSObject

/// The mode indicates whether test ads should be served instead
@property (nonatomic) BOOL testMode;

/// The identifier for the devices that will be used as test devices. The value for this will be printed in the debug console.
@property (nonatomic, nullable) NSArray *testDeviceIdentifiers;

/// The advertising identifier for this device
@property (nonatomic, readonly, nonnull) NSString *advertisingID;

/**
 The shared instance
 @return ADTRequestConfiguration
 */
+ (nonnull instancetype)sharedConfiguration;


@end
