//
//  main.m
//  ADTTest7
//
//  Created by Kyle M on 7/23/20.
//  Copyright © 2020 Infinite Peripherals. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
