//
//  ViewController.m
//  TestApp
//
//  Created by Kyle M on 10/24/19.
//  Copyright © 2019 ADTech. All rights reserved.
//

#import "ViewController.h"


// 1. import Ad-Tech
#import <ADTechMobile/ADTechMobile.h>

@interface ViewController ()  <ADTBannerViewDelegate, ADTInterstitialDelegate, ADTRewardedAdDelegate>

@property (strong, nonatomic) ADTBannerView *bannerView;
@property (strong, nonatomic) ADTInterstitial *interstitialAd;
@property (strong, nonatomic) ADTRewardedAd *rewardedAd;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [ADTRequestConfiguration sharedConfiguration].testMode = YES;
    //[ADTRequestConfiguration sharedConfiguration].testDeviceIdentifiers = @[@"put actual device id here"];
    
    [self setupBanner];
    
    [self setupInterstitial];
    
    [self setupRewardedVideoAd];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)setupBanner
{
    NSLog(@"Setup banner");
    
    // 3. Create banner
    self.bannerView = [[ADTBannerView alloc] initWithFrame:CGRectMake(0, UIScreen.mainScreen.bounds.size.height - 50, UIScreen.mainScreen.bounds.size.width, 50)];
    self.bannerView.adSize = height_50;
    self.bannerView.delegate = self;
    self.bannerView.rootViewController = self;
    [self.view addSubview:self.bannerView];
    [self.bannerView request];
    
    // 3. Create banner 2
    ADTBannerView *newBanner = [[ADTBannerView alloc] initWithFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, 50)];
    newBanner.adSize = height_50;
    newBanner.delegate = self;
    newBanner.rootViewController = self;
    [self.view addSubview:newBanner];
    [newBanner request];
    
    // 3. Create banner 3
    ADTBannerView *banner250 = [[ADTBannerView alloc] initWithFrame:CGRectMake(0, 100, UIScreen.mainScreen.bounds.size.width, 250)];
    banner250.adSize = height_250;
    banner250.delegate = self;
    banner250.rootViewController = self;
    [self.view addSubview:banner250];
    [banner250 request];
}

- (void)setupInterstitial
{
    self.interstitialAd = [[ADTInterstitial alloc] init];
    self.interstitialAd.delegate = self;
    [self.interstitialAd request];
}

- (void)setupRewardedVideoAd
{
    self.rewardedAd = [[ADTRewardedAd alloc] init];
    self.rewardedAd.delegate = self;
    [self.rewardedAd request];
}

- (IBAction)actionRewardedVideo:(id)sender
{
    if (self.rewardedAd.isReady) {
        [self.rewardedAd presentFromRootViewController:self];
    }
}

- (IBAction)actionInterstitial:(id)sender
{
    if (self.interstitialAd.isReady) {
        [self.interstitialAd presentFromRootViewController:self];
    }
}

#pragma mark - Banner delegate

- (void)adtBannerViewDidReceiveAd:(ADTBannerView *)bannerView
{
    NSLog(@"Banner view did receive ad");
}

- (void)adtBannerView:(ADTBannerView *)bannerView didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Banner view did fail to receive ad: %@", error.localizedDescription);
}

- (void)adtBannerViewWillPresentScreen:(ADTBannerView *)bannerView
{
    NSLog(@"Banner will present");
}

- (void)adtBannerViewWillDismissScreen:(ADTBannerView *)bannerView
{
    NSLog(@"Banner will dismiss");
}

- (void)adtBannerViewWillLeaveApplication:(ADTBannerView *)bannerView
{
    NSLog(@"Banner will leave");
}

#pragma mark - Interstitial delegate

- (void)adtInterstitialDidReceiveAd:(ADTInterstitial *)ad
{
    NSLog(@"Interstitial did receive ad");
}

- (void)adtInterstitial:(ADTInterstitial *)ad didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Interstitial did fail to receive ad: %@", error.localizedDescription);
}

- (void)adtInterstitialWillPresentScreen:(ADTInterstitial *)ad
{
    NSLog(@"Interstitial will present");
}

-(void)adtInterstitialWillDismissScreen:(ADTInterstitial *)ad
{
    NSLog(@"Interstitial will dismiss");
}

- (void)adtInterstitialDidDismissScreen:(ADTInterstitial *)ad
{
    NSLog(@"Interstitial did dismiss");
    
    [self.interstitialAd request];
}

- (void)adtInterstitialDidFailToPresentScreen:(ADTInterstitial *)ad
{
    NSLog(@"Interstitial failed to present");
}

- (void)adtInterstitialWillLeaveApplication:(ADTInterstitial *)ad
{
    NSLog(@"Interstiltial will leave");
}

#pragma mark - Rewarded delegate

- (void)adtRewardedAdDidReceiveAd:(ADTRewardedAd *)rewardedAd
{
    NSLog(@"Rewarded did receive ad");
}

- (void)adtRewardedAd:(ADTRewardedAd *)rewardedAd didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Rewarded failed: %@", error.localizedDescription);
}

- (void)adtRewardedAdDidPresent:(ADTRewardedAd *)rewardedAd
{
    NSLog(@"Rewarded did present");
}

- (void)adtRewardedAdDidEarnReward:(ADTRewardedAd *)rewardedAd
{
    NSLog(@"Rewarded did earn reward");
}

- (void)adtRewardedAdDidDismiss:(ADTRewardedAd *)rewardedAd
{
    NSLog(@"Rewarded did dismiss");
    
    [self.rewardedAd request];
}
@end
