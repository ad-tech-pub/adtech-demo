//
//  NativeViewController.m
//  ADTechMobile
//
//  Created by Kyle M on 12/13/19.
//  Copyright © 2019 ADTech. All rights reserved.
//

#import "NativeViewController.h"


@interface NativeViewController () <ADTNativeAdDelegate>

/// The headline label outlet
@property (weak, nonatomic) IBOutlet UILabel *headlineLabel;
/// The action button
@property (weak, nonatomic) IBOutlet UIButton *callToActionButton;
/// The ad icon image view
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
/// The native ad handle
@property (strong, nonatomic) ADTNativeAd *nativeAd;

@end

@implementation NativeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.nativeAd = [[ADTNativeAd alloc] init];
    self.nativeAd.delegate = self;
    [self.nativeAd request];
}

- (IBAction)actionClose:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Delegates
- (void)adtNativeAdDidReceiveAd:(ADTNativeAd *)ad
{
    // Set handle to the returned native ad
    self.nativeAd = ad;
    
    // Create the media ad view to show ad media, could be a video or an image
    UIView *mediaAdView = [[UIView alloc] initWithFrame:CGRectMake(0, 400, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height - 400)];
    // Add the media ad view to super view for displaying
    [self.view addSubview:mediaAdView];
    
    // Register views for the native ad, and register the views that are clickable
    [self.nativeAd registerAdView:self.view
                        mediaView:mediaAdView
                   viewController:self
                   clickableViews:@{
                       ADTNativeAdHeadlineAsset: self.headlineLabel,
                       ADTNativeAdIconAsset: self.iconImageView,
                       ADTNativeAdCallToActionAsset: self.callToActionButton
                   }];
        
    // Display headline if available
    if (self.nativeAd.headline) {
        self.headlineLabel.text = self.nativeAd.headline;
    }
    
    // Set the call to action button text
    if (self.nativeAd.callToAction) {
        [self.callToActionButton setTitle:self.nativeAd.callToAction forState:UIControlStateNormal];
    }
    
    // Display the icon for this ad
    if (self.nativeAd.icon) {
        self.iconImageView.image = self.nativeAd.icon;
    }
    
    // Display all text
    NSMutableString *string = [NSMutableString new];
    [string appendFormat:@"-> headline: %@\n", self.nativeAd.headline ? self.nativeAd.headline : @""];
    [string appendFormat:@"-> callToAction: %@\n", self.nativeAd.callToAction ? self.nativeAd.callToAction : @""];
    [string appendFormat:@"-> body: %@\n", self.nativeAd.body ? self.nativeAd.body : @""];
    [string appendFormat:@"-> store: %@\n", self.nativeAd.store ? self.nativeAd.store : @""];
    [string appendFormat:@"-> price: %@\n", self.nativeAd.price ? self.nativeAd.price : @""];
    [string appendFormat:@"-> advertiser: %@\n", self.nativeAd.advertiser ? self.nativeAd.advertiser : @""];
    [string appendFormat:@"-> iconURL: %@\n", self.nativeAd.iconURL ? self.nativeAd.iconURL.absoluteString : @""];
    
    self.headlineLabel.text = string;
    if (self.nativeAd.icon) {
        self.iconImageView.image = self.nativeAd.icon;
    }
    else if (self.nativeAd.iconURL) {
        [self.nativeAd loadImageAsync:self.nativeAd.iconURL completion:^(UIImage * _Nullable image) {
            self.iconImageView.image = image;
        }];
    }
}

/// Tell delegate native ad has failed
- (void)adtNativeAd:(ADTNativeAd *)ad didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"adtNativeAd failed, error: %@", error.localizedDescription);
}

/// Tell delegate that native ad did record impression from showing the ad
- (void)adtNativeAdDidRecordImpression:(ADTNativeAd *)ad
{
    NSLog(@"adtNativeAdDidRecordImpression");
}

/// Tell delegate native ad did finish loading all assets
- (void)adtNativeAdDidFinishLoading:(ADTNativeAd *)ad
{
    NSLog(@"adtNativeAdDidFinishLoading");
}

/// Tell delegate that native ad did record user tap
- (void)adtNativeAdDidRecordClick:(ADTNativeAd *)ad
{
    NSLog(@"adtNativeAdDidRecordClick");
}

/// Tell delegate that native ad will present a new ad screen
- (void)adtNativeAdWillPresentScreen:(ADTNativeAd *)ad
{
    NSLog(@"adtNativeAdWillPresentScreen");
}

/// Tell delegate that native ad did successfully present new ad screen
- (void)adtNativeAdDidPresentScreen:(ADTNativeAd *)ad
{
    NSLog(@"adtNativeAdDidPresentScreen");
}

/// Tell delegate that native ad will dismiss the ad screen being displayed
- (void)adtNativeAdWillDismissScreen:(ADTNativeAd *)ad
{
    NSLog(@"adtNativeAdWillDismissScreen");
}

/// Tell delegate that native ad did dismiss the ad screen
- (void)adtNativeAdDidDismissScreen:(ADTNativeAd *)ad
{
    NSLog(@"adtNativeAdDidDismissScreen");
}

/// Tell delegate that application will go to background, and transition to a new app
- (void)adtNativeAdWillLeaveApplication:(ADTNativeAd *)ad
{
    NSLog(@"adtNativeAdWillLeaveApplication");
}

@end
