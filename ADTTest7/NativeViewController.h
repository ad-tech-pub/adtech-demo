//
//  NativeViewController.h
//  ADTechMobile
//
//  Created by Kyle M on 12/13/19.
//  Copyright © 2019 ADTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ADTechMobile/ADTechMobile.h>

NS_ASSUME_NONNULL_BEGIN

@interface NativeViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
